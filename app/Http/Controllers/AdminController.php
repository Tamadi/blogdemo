<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class AdminController extends Controller

{
    
    public function index()
    {
        $users = \App\User::all();

        $data['users'] = $users;

        return view('admin.index', $data);
        
    }
    public function edit($id)

    {
        $user = User::find($id);

        $data['user'] = $user;
        $data['roles']= Role::all();

        return view('admin.edit', $data);
    }
    public function update(request $request, $id)
    
        {
        //dd($request);

        $validator = Validator::make($request->all(), [

            'name' => 'required',
            'email' => 'required',
            'role_id'=>'required|numeric', 
        ]);


        if ($validator->fails()) {
            return redirect('/user/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
            }

        $user = User::find($id);
        

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role_id = $request->input('role_id');

        $user->save(); 
            return redirect ('/admin/');
        }

    public function delete($id)
     
     {
    
        if (Auth::user()->id = $id){
            User::destroy($id);
        }

        return redirect('/admin');
    }
}