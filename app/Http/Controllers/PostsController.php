<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use \App\Post;
use Validator;
use Illuminate\Support\Facades\Auth;



class Postscontroller extends Controller
{
    //
	public function index()
    {
		$posts = Post::all();
		// dd($posts);

		// foreach ($posts as $post) {
		// 	echo $post->title."<br>";
		// }

		$data['posts'] = $posts;

		return view('posts.index', $data);
		
    }

    public function show($id)
    {
    	$post = Post::find($id);

    	// dd($post);
        $data['comments']= $post->comments;
    	$data['post'] = $post;

    	return view('posts.show', $data);
    }

    public function edit($id)
    {
        $post = Post::find($id);

        $data['post'] = $post;

        return view('posts.edit', $data);
    }

    public function update(Request $request, $id)
    {
        //dd($request);

        $validator = Validator::make($request->all(), [

            'title' => 'required|max:255',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/post/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }
        $post = Post::find($id);
        


        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');

        if ($post->save()) {
            return redirect ('/posts/'.$id.'/edit');
        } 

    }
    public function store(Request $request)
    {
    	//dd($request);

        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:posts|max:255',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/post')
                        ->withErrors($validator)
                        ->withInput();
        }


    	$post = new Post;
    	$post->title = $request->input('title');
    	$post->body = $request->input('body');

    	if ($post->save()) {
    		return redirect ('/posts');
    	} else {
    		
        }


    	//$post->title = "I like popsicles";
    	//$post->body = "Something about popsicles Something about popsicles Something about popsicles Something about popsicles Something about popsicles Something about popsicles Something about popsicles. ";

    	//$post->save();

    }

	public function create ()
	{
			return view('posts.create');
	}

    public function storeComment(Request $request)
    {
        $comment = new \App\Comment;
        $comment->user_id = Auth::user()->id;
        $comment->post_id =$request->input('post_id');
        $comment->body=$request->input('body');

        $comment->save();

        return redirect('/post/'.$request->input('post_id'));
    }
}