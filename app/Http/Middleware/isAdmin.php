<?php

namespace App\Http\Middleware;
use illuminate\Support\Facades\Auth;


use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role->name !="admin"){
            return redirect ('/');
        }
        return $next($request);
    }
}
