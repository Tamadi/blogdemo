<?php

namespace App\Http\Middleware;
use Jenssegers\Agent\Agent;

use Closure;

class DetectMobile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$agent = new Agent();
    	if ($agent->isMobile()){
    		return redirect('/mobile');
    	}

    	return $next($request);
       // if (Auth::guard($guard)->check()) {
        //    return redirect('/index');
       // }
       // return $next($request);
    }
}
