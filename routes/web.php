<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
 //   return view('welcome');
//});
Route::get('/detect', 'PostController@agent');
Route::get('/mobile', 'PostController@index');
Route::get('/', 'PostController@index')->middleware('mobile');
Route::get('/about', 'PostController@about');
Route::get('/blog', 'PostController@blog');
Route::get('/lifestyle', 'PostController@lifestyle');
Route::get('/contact', 'PostController@contact');

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/admin/login', 'AdminController@login');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@index')    
    ->middleware('authenticated', 'admin');
Route::get('/posts', 'PostsController@index');
Route::get('post/{id}/edit', 'PostsController@edit');
Route::get('/post/{id}', 'PostsController@show');


Route::get('/newpost', 'PostsController@store');
Route::get('/post', 'PostsController@create');
Route::post('/post/{id}', 'PostsController@update');
Route::post('/post', 'PostsController@store');
Route::post('/comment', 'PostsController@storeComment');
Route::get('admin/{id}/edit', 'AdminController@edit')->middleware('authenticated', 'admin');
Route::post('/admin/{id}, AdminController@update')->middleware ('authenticated', 'admin');
Route::get('/admin/{id}, AdminController@delete')->middleware('admin');
//Route::get('/admin', 'PostController@showAdmin')->middleware('authenticated');
//Route::get('admin', ['middleware' => 'adminarea', 'uses' => 'AdminController@index']);


