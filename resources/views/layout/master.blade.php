<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Paper &mdash; Free Website Template, Free HTML5 Template by freehtml5.co</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Free HTML5 Website Template by freehtml5.co" />
		<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
		<meta name="author" content="freehtml5.co" />
		<!--
		//////////////////////////////////////////////////////
		FREE HTML5 TEMPLATE
		DESIGNED & DEVELOPED by FreeHTML5.co
			
				Website: 		http://freehtml5.co/
					Email: 			info@freehtml5.co
				Twitter: 		http://twitter.com/fh5co
				Facebook: 		https://www.facebook.com/fh5co
		//////////////////////////////////////////////////////
		-->
		<!-- Facebook and Twitter integration -->
		<meta property="og:title" content=""/>
		<meta property="og:image" content=""/>
		<meta property="og:url" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content=""/>
		<meta name="twitter:title" content="" />
		<meta name="twitter:image" content="" />
		<meta name="twitter:url" content="" />
		<meta name="twitter:card" content="" />
		<link href="https://fonts.googleapis.com/css?family=Josefin+Slab:400,600,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
		
		<!-- Animate.css -->
		<link rel="stylesheet" href="{{ url('/') }}/css/animate.css">
		<!-- Icomoon Icon Fonts-->
		<link rel="stylesheet" href="{{ url('/') }}/css/icomoon.css">
		<!-- Bootstrap  -->
		<link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.css">
		<!-- Magnific Popup -->
		<link rel="stylesheet" href="{{ url('/') }}/css/magnific-popup.css">
		<!-- Flexslider  -->
		<link rel="stylesheet" href="{{ url('/') }}/css/flexslider.css">
		<!-- Theme style  -->
		<link rel="stylesheet" href="{{ url('/') }}/css/style.css">
		<!-- Modernizr JS -->
		<script src="js/modernizr-2.6.2.min.js"></script>
		<!-- FOR IE9 below -->
		<!--[if lt IE 9]>
		<script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		
		<div class="fh5co-loader"></div>
		
		<div id="page">
			@include('layout.nav')
			
			@yield('content')

			@include('layout.footer')
			
		</div>
		<div class="gototop js-top">
			<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
		</div>
		
		<!-- jQuery -->
		<script src="{{ url('/') }}/js/jquery.min.js"></script>
		<!-- jQuery Easing -->
		<script src="{{ url('/') }}/js/jquery.easing.1.3.js"></script>
		<!-- Bootstrap -->
		<script src="{{ url('/') }}/js/bootstrap.min.js"></script>
		<!-- Waypoints -->
		<script src="{{ url('/') }}/js/jquery.waypoints.min.js"></script>
		<!-- Flexslider -->
		<script src="{{ url('/') }}/js/jquery.flexslider-min.js"></script>
		<!-- Magnific Popup -->
		<script src="{{ url('/') }}/js/jquery.magnific-popup.min.js"></script>
		<script src="{{ url('/') }}/js/magnific-popup-options.js"></script>
		<!-- Main -->
		<script src="{{ url('/') }}/js/main.js"></script>
	</body>
</html>