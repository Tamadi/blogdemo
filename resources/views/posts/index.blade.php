@extends('layout.master')

@section('content')

<section>

	<div class="fh5co-about animate-box">
		<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
			<h2>All Posts</h2>			
		</div>	

		@foreach($posts as $post)
		<div class="container" style="margin-bottom: 20px;">
			<div class="col-md-8 col-md-offset-2 animate-box">
				<h2 class="pull-left">{{ $post->title }}</h2><br><br>
				<small class="pull-left" style="position: relative; top: 12px; left: 5px;">
					<a href="{{ url( '/post/'.$post->id ) }}">View Post</a>
				</small>
				@guest
				@else
				<small class="pull-left" style="position: relative; top: 12px; left: 5px; margin-left: 5px;">
					<a href="{{ url( '/post/'.$post->id.'/edit' ) }}">&nbsp Edit Post</a>
				</small><br>
				@endguest
				<div class="container" style="margin-bottom: 10px;">
					<p class="pull-left">{{ $post->body }}</p><br><br>
				</div>
			</div>
		</div>
		@endforeach

	</div>
	
	
</section>

@endsection