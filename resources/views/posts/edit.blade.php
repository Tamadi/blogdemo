@extends('layout.master')


@section('content')
<div class="container" style="margin-bottom: 30px;">
	<div class="row">
		<h2>Edit Post</h2>
		<div class="col-md-9 col-padded-right">
			<form method="post" action="{{ url('/post/'.$post->id) }}">
				@csrf
				<div class="form-group row">
					<div class="col-md-6 field">
						<label for="title">Title</label>
						 <input type="text" name="title" id="posttitle" class="form-control" value="{{ (old('title') ) ? old('title') : $post->title }}">
						@if ($errors->has('title'))
							<span class="invalid feedback" role="alert">
								<strong>{{ $errors->first('title') }}</strong>
							</span>
						@endif
					</div>
				</div>
				
			</div>
			<div class="form-group row">
				<div class="col-md-12 field">
					<label for="message">Post Body</label>
					<textarea class="form-control" id="message" cols="30" rows="10" name="body">{{ (old('body') ) ? old('body') : $post->body}}</textarea>
					@if ($errors->has('body'))
						<span class="invalid-feedback" role="alert">
						<strong>{{
							$errors->first('body')
						}}</strong>
						</span>
					@endif
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-12 field">
					<input type="submit" id="submit" class="btn btn-primary" value="Update">
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
