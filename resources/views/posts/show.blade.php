@extends('layout.master')


@section('content')
	
<section>

	<div class="fh5co-about animate-box">
		<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
			<h2>{{ $post->title }}</h2>			
		</div>	

		
		<div class="container" style="margin-bottom: 25px;">
			<div class="col-md-8 col-md-offset-2 animate-box">				
				<p class="pull-left">{{ $post->body }}</p>
				<div>
				<p> <ul>
					<ul><a href="{{ url('/posts/') }}">Back to All post</a></ul>
					<ul><a href="{{ url('/post/') }}">Create a post</a></ul>
				<ul>
				</p>
				</div>

			<div class="container" style="margin-bottom: 25px;">
				@foreach($comments as $comment)
				<div>
					<p>Author:{{$comment->user->name}}</p>
				</div> <br>
				<p>{{ $comment->body}}</p> <br>
				@endforeach
			</div>
		</div>
		@guest	
			<p>Please log in to comment</p>
			@else
			<form action = "{{ url('/comment') }}" method="post">
				@csrf
				<input type="hidden" name="post_id" value="{{ $post->id }}"><br>
				<textarea name ="body"></textarea><br>
				<button type="submit">Submit Comment</button>
			</form>
		</div>
		@endguest


	</div>
	
	
</section>

@endsection