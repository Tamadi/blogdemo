@extends('layout.master')

@section('content')
<div class="container-fluid"> 
    <ul>
        <p>Welcome,{{Auth::user()->name }}</p>
    </ul>
        <div class="card-body">
            <div class="form-group row">
                <form method="POST" action="{{ url('/logout') }}">
                        @csrf
                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Logout') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Dashboard</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            You are logged in!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
