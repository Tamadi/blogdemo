@extends('layout.master')
@section ('content')

<div class="container" style="margin-bottom: 30px;">
	<div class="row">
		<h2>Edit Post</h2>
		<div class="col-md-9 col-padded-right">
			<form method="post" action="{{ url('/admin/'.$user->id) }}">
				@csrf
				<div class="form-group row">
					<div class="col-md-6 field">
						<label for="name">Name</label>
						 <input type="text" name="name" id="username" class="form-control" value="{{ (old('name') ) ? old('title') : $user->name }}">
						@if ($errors->has('name'))
							<span class="invalid feedback" role="alert">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
						@endif
					<div class="col-md-6 field">
						<label for="email">Email</label>
						 <input type="text" name="email" id="useremail" class="form-control" value="{{ (old('email') ) ? old('email') : $user->email }}">
						@if ($errors->has('name'))
							<span class="invalid feedback" role="alert">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group row">
				<div class="col-md-12 field">
						<label>Role</label>
						<select name ="role_id" class="form-control">
							@foreach ($roles as $role)
								<option value = "{{$role->id}}" @if($user->role->id==$role->id) selected="selected"
							@endif>
							{{ucfirst($role->name)}}</option>
							@endforeach
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-md-12 field">
						<input type="submit" id="submit" class="btn btn-primary" value="Update">
					</div>
				</div>
			</form>
	</div>
</div>
@endsection