@extends('layout.master')

@section('content')

	
</div>
<div id="fh5co-contact" class="fh5co-no-pd-top">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-12 col-md-offset-0 text-center fh5co-heading">
					<h2><span>All Users</span></h2>
				</div>
				<div class="container" style="margin-bottom: 30px;">
					<div class="col-md-9 col-padded-right">
				 		<h3>Email</h3>
					</div>
					<div class="col-md-9 col-padded-left">
					 	<h3>Role</h3>
					</div>
					<div class="col-md-9 col-padded-left">
					 	<h3>Options</h3>
					</div>
				</div>
			<div class="row">
				@foreach($users as $user)
				<div class="col-md-9 col-padded-right">
						<div class="form-group row">
							<div class="col-md-6 field">
								<h3>{{ $user->name }}</h3>	
							</div>
							<div class="col-md-6 field">
								<h3>{{ $user->email }}</h3>
							</div>

							<div class="col-md-6 field">
								<h3>{{ $user->role->name }}</h3>
							</div>
						</div>
							<div class="form-group row">
								<div class="col-md-12 field">

									<h3>
										<a href="{{ url('/admin/'.$user->id.'/edit') }}">Edit</a>
										<a href="{{ url('/admin/'.$user->id.'/delete') }}">Delete</a>
									</h3>
								</div>
							</div>
					</div>
					@endforeach
				</div>
				

@endsection
