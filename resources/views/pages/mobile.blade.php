about.@extends('layout.master')
@section('content')

/**
3
 * MIT License
4
 * ===========
5
 *
6
 * Permission is hereby granted, free of charge, to any person obtaining
7
 * a copy of this software and associated documentation files (the
8
 * "Software"), to deal in the Software without restriction, including
9
 * without limitation the rights to use, copy, modify, merge, publish,
10
 * distribute, sublicense, and/or sell copies of the Software, and to
11
 * permit persons to whom the Software is furnished to do so, subject to
12
 * the following conditions:
13
 *
14
 * The above copyright notice and this permission notice shall be included
15
 * in all copies or substantial portions of the Software.
16
 *
17
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
18
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
19
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
20
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
21
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
22
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
23
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
24
 *
25
 * -----------------------------------------------------------------------
26
 * The demo is running all the Mobile_Detect's internal methods.
27
 * Here you can spot detections errors instantly.
28
 * -----------------------------------------------------------------------
29
 *
30
 * @author      Serban Ghita <serbanghita@gmail.com>
31
 * @license     MIT License https://github.com/serbanghita/Mobile-Detect/blob/master/LICENSE.txt
32
 *
33
 */
34
​
35
require_once '../Mobile_Detect.php';
36
$detect = new Mobile_Detect;
37
​
38
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
39
$scriptVersion = $detect->getScriptVersion();
40
​
41
?><!DOCTYPE html>
42
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
43
<head>
44
    <meta charset="utf-8">
45
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
46
    <title>Mobile Detect Local Demo</title>
47
    <style type="text/css">
48
        html { font-size: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
49
        body { margin: 0; padding: 0 1em; font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 1em; color: #333333; background-color: #ffffff; max-width:320px; }
@fehintolu12
Propose file change
Commit summary 
Update demo.php
Optional extended description

Add an optional extended description…
 
© 2018 GitHub, Inc.
Terms
Privacy
Security
Status
Help
Contact GitHub
Pricing
API
Training
Blog
About
Press h to open a hovercard with more details.

