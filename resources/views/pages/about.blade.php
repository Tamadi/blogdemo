about.@extends('layout.master')
@section('content')
<div id="fh5co-content" class="fh5co-no-pd-top">
	<div class="container">
		<div class="row animate-box">
			<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
				<h2><span>About Us</span></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="fh5co-staff">
					<img src="images/user-2.jpg" alt="Free HTML5 Templates by FreeHTML5.co">
					<h3>Jean Smith</h3>
					<strong class="role">CEO, Founder</strong>
					<p>Quos quia provident conse culpa facere ratione maxime commodi voluptates id repellat velit eaque aspernatur expedita.</p>
					<ul class="fh5co-social-icons">
						<li><a href="#"><i class="icon-facebook"></i></a></li>
						<li><a href="#"><i class="icon-twitter"></i></a></li>
						<li><a href="#"><i class="icon-dribbble"></i></a></li>
						<li><a href="#"><i class="icon-github"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
@endsection