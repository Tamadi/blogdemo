show.@extends('layout.master')

@section('title', 'One Specific Post')

@section('content')

<div class="title m-b-md">
    Showing post # {{ $id }} <br>

    Your name is {{$name}}
</div>

@endsection